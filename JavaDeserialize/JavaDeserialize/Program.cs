﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace JavaDeserialize
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            var scaFilePath = "../../../sca";
            var javaExFilePath = "../../../javaListEx.ser";

            //var b = File.OpenRead(scaFilePath);
            var b = File.OpenRead(javaExFilePath);

            Console.WriteLine("Found " + b.Length.ToString() + " bytes");


            /*            // Try just doing naive IP- regex
                        naiveRegex(scaFilePath);
            */

            parseSerializedBytes(b);
            //testShortUtf();
            //testLongUtf();

        }

        public static int handleCounter = JavaConstants.baseHandle;
        public static Dictionary<int, JContent> handleDict = new Dictionary<int, JContent>();

        public class JavaConstants
        {
            public static byte[] MAGIC = { 0xAC, 0xED };
            public static byte[] STREAM_VER = { 0x00, 0x05 };
            public static int baseHandle = 0x007E0000;
        }

        public const byte TC_NULL             = 0x70; 
        public const byte TC_REFERENCE        = 0x71; 
        public const byte TC_CLASSDESC        = 0x72; 
        public const byte TC_OBJECT           = 0x73; 
        public const byte TC_STRING           = 0x74; 
        public const byte TC_ARRAY            = 0x75; 
        public const byte TC_CLASS            = 0x76; 
        public const byte TC_BLOCKDATA        = 0x77;
        public const byte TC_ENDBLOCKDATA     = 0x78;
        public const byte TC_RESET            = 0x79;
        public const byte TC_BLOCKDATALONG    = 0x7A;
        public const byte TC_EXCEPTION        = 0x7B;
        public const byte TC_LONGSTRING       = 0x7C;
        public const byte TC_PROXYCLASSDESC   = 0x7D;
        public const byte TC_ENUM             = 0x7E;

        public const byte PTC_BYTE            = (byte) 'B';
        public const byte PTC_CHAR            = (byte) 'C';
        public const byte PTC_DOUBLE          = (byte) 'D';
        public const byte PTC_FLOAT           = (byte) 'F';
        public const byte PTC_INTEGER         = (byte) 'I';
        public const byte PTC_LONG            = (byte) 'J';
        public const byte PTC_SHORT           = (byte) 'S';
        public const byte PTC_BOOLEAN         = (byte) 'Z';

        public const byte OTC_ARRAY           = (byte) '[';
        public const byte OTC_OBJECT          = (byte) 'L';

        public static readonly byte[] PRIM_TC = {PTC_BYTE, PTC_CHAR, PTC_DOUBLE, PTC_FLOAT, PTC_INTEGER, PTC_LONG, PTC_SHORT, PTC_BOOLEAN};
        public static readonly byte[] OBJ_TC = { OTC_ARRAY, OTC_OBJECT };

        public interface JContent
        {
            public byte getTC();
            public int getHandle();
            public void setHandle(int handle);
        }
        public class BaseContent : JContent
        {
            public byte getTC() { return tc; }
            public int getHandle() { return handle; }
            public void setHandle(int handle) { this.handle = handle; }

            byte tc;
            public int handle;
        }

        static void parseSerializedBytes(FileStream fstream)
        {
            var stream = new BinaryReader(fstream);

            // Verify magic number 
            if (!JavaConstants.MAGIC.SequenceEqual(stream.ReadBytes(2))) {
                Console.WriteLine("Could not verify magic number");
                return;
            }

            // Verify stream version
            if (!JavaConstants.STREAM_VER.SequenceEqual(stream.ReadBytes(2))) {
                Console.WriteLine("Stream version does not match");
                return;
            }

            Console.WriteLine("Trying to read contents");

            // Try to parse constants
            List<JContent> contents = parseContents(stream);

        }

        static List<JContent> parseContents(BinaryReader stream)
        {
            //contents:
            //  content
            //  contents content
            try
            {
                List<JContent> contents = new List<JContent>();

                while (true)
                {
                    try {
                        var content = parseContent(stream);
                        contents.Add(content);
                    } catch (Exception e)
                    {
                        break;
                    }
                }

                return contents;
            } catch(Exception e)
            {
                Console.WriteLine("Could not parse a content");
                throw e;
            }
        }

        static JContent parseContent(BinaryReader stream)
        {
            byte tc = stream.ReadByte();
            Console.WriteLine($"parsing content: {tc:X}");
            switch(tc)
            {
                case TC_OBJECT:
                    return parseNewObject(stream);
                case TC_ENDBLOCKDATA:
                    throw new EndBlockFoundException();
                default:
                    throw new NotImplementedException($"{tc:X} not yet implemented");
            }
        }

        public class JObject : BaseContent
        {
            public JContent classDesc;
        }
        static JObject parseNewObject(BinaryReader stream)
        {
            Console.WriteLine($"parsing newObject");
            JObject obj = new JObject();

            // classDesc
            obj.classDesc = parseClassDesc(stream);

            // newHandle
            newHandle(obj);

            // classdata[]
            // Get data for each class... do we need to traverse tree of superclasses?
            Console.WriteLine($"NOT SURE ABOUT classdata[]");
            // WHY ISNT EXCEPTION BEING CAUGHT or recognixed?
            throw new NotImplementedException("Not sure how to parse classdata[]");

            return obj;
        }

        static JContent parseClassDesc(BinaryReader stream)
        {
            byte tc = stream.ReadByte();
            Console.WriteLine($"parsing classDesc: {tc:X}");
            switch(tc)
            {
                // newClassDesc
                case TC_CLASSDESC:
                    return parseNewClassDesc(stream);
                case TC_PROXYCLASSDESC:
                    return parseNewClassDescProxy(stream);
                // nullReference
                case TC_NULL:
                    return null;
                // prevObject
                case TC_REFERENCE:
                    throw new NotImplementedException("prevObject (TC_REFERENCE) in parseClassDesc not yet implemented");
            }

            return null;
        }

        public class JClassDesc : BaseContent
        {
            public string className;
            public long serialVersionUID;
            public byte flags;
            public Dictionary<string, JField> fields;
            public List<JContent> annotations;
            public JContent superClassDesc;
        }
        static JContent parseNewClassDesc(BinaryReader stream)
        {
            Console.WriteLine($"parsing newClassDesc");
            JClassDesc classDesc = new JClassDesc();

            // className
            classDesc.className = parseShortUtf(stream);

            // serialVersionUID
            classDesc.serialVersionUID = parseLong(stream);

            // newHandle
            newHandle(classDesc);

            // classDescInfo
            parseClassDescInfo(classDesc, stream);

            return null;
        }

        static void parseClassDescInfo(JClassDesc classDesc, BinaryReader stream)
        {
            Console.WriteLine($"parsing classDescInfo");
            // classDescFlags
            classDesc.flags = stream.ReadByte();
            // fields
            classDesc.fields = parseFields(stream);
            // classAnnotation
            classDesc.annotations = parseClassAnnotations(stream);
            // superClassDesc
            classDesc.superClassDesc = parseClassDesc(stream);
        }

        public class JField
        {
            public byte ptc;
            public string fieldName;
            public JString className1 = null;
        }
        static Dictionary<string, JField> parseFields(BinaryReader stream)
        {
            var fieldMap = new Dictionary<string, JField>();
            short count = parseShort(stream);
            for (int i=0; i < count; i++)
            {
                JField field = new JField();
                // parse fieldDesc
                // typecode - prim_typecode or obj_typecode
                field.ptc = stream.ReadByte();
                // fieldName
                field.fieldName = parseShortUtf(stream);

                // className1, if applicable
                if (OBJ_TC.Contains(field.ptc))
                {
                    field.className1 = parseStringObject(stream);
                }

                // Add to dict
                fieldMap.Add(field.fieldName, field);
            }

            return fieldMap;
        }
        public class JString : BaseContent
        {
            public string value;
        }
        public static JString parseStringObject(BinaryReader stream)
        {
            JString strobj = new JString();
            // can be newString or prevObject (or Null ?)    
            byte tc = stream.ReadByte();
            switch(tc)
            {
                // newString
                case TC_STRING:
                    newHandle(strobj);
                    strobj.value = parseShortUtf(stream);
                    Console.Write($"Parsed string object with value {strobj.value}\n");
                    return strobj;
                case TC_LONGSTRING:
                    newHandle(strobj);
                    strobj.value = parseLongUtf(stream);
                    Console.Write($"Parsed string object with value {strobj.value}\n");
                    return strobj;
                // prevObject
                case TC_REFERENCE:
                    int handle = parseInt(stream);
                    return (JString) handleDict[handle];
                default:
                    throw new Exception($"parseStringObject found {tc:X}?");
            }
        }
        static List<JContent> parseClassAnnotations(BinaryReader stream)
        {
            var contents = new List<JContent>();

            while(true)
            {
                try
                {
                    contents.Add(parseContent(stream));
                } catch(EndBlockFoundException eb)
                {
                    break;
                }
            }

            return contents;
        }

        static JContent parseNewClassDescProxy(BinaryReader stream)
        {
            // newHandle

            // proxyClassDescInfo
            return null;
        }

        static int newHandle(BaseContent content)
        {
            Console.WriteLine($"newHandle - {content.getTC()}: {handleCounter:X8}");
            content.setHandle(handleCounter);
            handleDict.Add(handleCounter, content);
            return handleCounter++;
        }
        static void resetHandle()
        {
            handleCounter = JavaConstants.baseHandle;
        }

        static string parseUtfString(BinaryReader stream)
        {
            byte stringType = stream.ReadByte();
            switch (stringType)
            {
                case TC_STRING:
                    return parseShortUtf(stream);
                case TC_LONGSTRING:
                    return parseLongUtf(stream);
                default:
                    throw new Exception($"Unrecognized typecode for utf string: {stringType:X}");
            }
        }
        static string parseShortUtf(BinaryReader stream)
        {
            // the first two bytes represent the length of the string
            byte[] lengthb = stream.ReadBytes(2);
            int length = bytesToShort(lengthb);

            Console.WriteLine($"Found length {length}");

            // Grab that many bytes from the stream and return as string
            byte[] chars = stream.ReadBytes(length);
            return Encoding.ASCII.GetString(chars);
        }

        static short bytesToShort(byte[] bytes)
        {
            return BitConverter.ToInt16(endianCheck(bytes), 0);
        }
        static int bytesToInt(byte[] bytes)
        {
            return BitConverter.ToInt32(endianCheck(bytes), 0);
        }
        static long bytesToLong(byte[] bytes)
        {
            return BitConverter.ToInt64(endianCheck(bytes), 0);
        }
        static long parseLong(BinaryReader stream)
        {
            return bytesToLong(stream.ReadBytes(8));
        }
        static int parseInt(BinaryReader stream)
        {
            return bytesToInt(stream.ReadBytes(4));
        }
        static short parseShort(BinaryReader stream)
        {
            return bytesToShort(stream.ReadBytes(2));
        }
        static byte[] endianCheck(byte[] bytes)
        {
            if (BitConverter.IsLittleEndian) {
                Array.Reverse(bytes);
            }
            return bytes;
        }


        static string parseLongUtf(BinaryReader stream)
        {
            // the first two bytes represent the length of the string
            byte[] lengthb = stream.ReadBytes(8);
            long length = bytesToLong(lengthb);

            // Grab that many bytes from the stream and return as string
            int int32len;
            var sb = new StringBuilder();
            while (length > 0)
            {
                if (length > int.MaxValue)
                {
                    int32len = int.MaxValue;
                    length -= int.MaxValue;
                } else
                {
                    int32len = (int) length;
                    length -= length;
                }

                byte[] chars = stream.ReadBytes(int32len);
                sb.Append(Encoding.ASCII.GetString(chars));
            }

            return sb.ToString();
        }

        static void naiveRegex(string scaFilePath)
        {
            var sca = File.ReadAllText(scaFilePath);
            var IPRE = @"IP-\w{8}";

            var ipre = new Regex(IPRE, RegexOptions.Compiled);

            var matches = ipre.Matches(sca);

            Console.WriteLine($"Found {matches.Count} matches");
        }

        static void testShortUtf()
        {
            byte[] testb = { 0x00, 0x04, 0x48, 0x61, 0x73, 0x68, 0x68 /*extra byte*/ };
            string tests = "Hash";

            string result = parseShortUtf(new BinaryReader(new MemoryStream(testb)));

            Console.WriteLine($"Found '{result}', expected '{tests}'");
        }
        static void testLongUtf()
        {
            byte[] testb = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x48, 0x61, 0x73, 0x68, 0x68 /*extra byte*/ };
            string tests = "Hash";

            string result = parseLongUtf(new BinaryReader(new MemoryStream(testb)));

            Console.WriteLine($"Found '{result}', expected '{tests}'");
        }

        public class EndBlockFoundException : Exception { };
    }


}
